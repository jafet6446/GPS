<!DOCTYPE html>

<html>

<head>
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style4.css">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="CSS/DashStyles.css"> 

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
	<link href="{{ asset('css/DashStyles.css') }}" rel="stylesheet">
	
    <title>Tablero</title>

    <link href="{{ asset('css/lista.css') }}" rel="stylesheet">

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>GPS Tracking System</h3>
                <strong>GTS</strong>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                    <a href="{{url('home')}}">
                        <i class='bx bx-home'></i>
                        Inicio
                    </a>
                </li>
				
				@if(Auth::user()->hasRol('Usuario'))
                <li>
                    <a href="{{ route('perfil.index') }}">
                        <i class='bx bx-user'></i>
                        Perfil
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Usuario'))
                <li>
                    <a href="{{ route('vehiculo.index') }}">
                        <i class='bx bx-car'></i>
                        Mis Vehículos
                    </a>
                </li>
				
				@endif
				@if(Auth::user()->hasRol('Usuario'))
                <li>
                    <a href="{{ route('emergencias.index') }}">
                        <i class='bx bx-phone-call bx-tada nav_icon'></i>
                        Emergencia 
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				 <li>
                    <a href="{{ route('clase.index') }}">
                        <i class='bx bx-car'></i>
                        Tipos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('marca.index') }}">
                        <i class='bx bx-car'></i>
                        Marcas
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('modelo.index') }}">
                        <i class='bx bx-car'></i>
                        Modelos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('paquetes.index') }}">
                        <i class='bx bx-car'></i>
                        Paquetes
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('notificacion.index') }}">
                        <i class='bx bx-car'></i>
                        Notificaciones
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('emergencias.lista') }}">
                        <i class='bx bx-car'></i>
                        Emergencias
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('usuarios.index') }}">
                        <i class='bx bx-car'></i>
                        Usuarios
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('formulario.index') }}">
                        <i class='bx bx-car'></i>
                        Solicitudes
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('gps.vehiculos') }}">
                        <i class='bx bx-car'></i>
                        Vehículos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('gps.index') }}">
                        <i class='bx bx-car'></i>
                        GPS
                    </a>
                </li>
				@endif
            </ul>

                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class='bx bx-log-out nav_icon'></i> {{ __('Cerrar sesión') }}</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                       @csrf
                    </form>

                                   
        </nav>

        <!-- Page Content  -->
        <div id="content">
            

                    
                    
                
            <div id="googleMap" style="width:100%;height:500px;"></div>
			
        </div>
    </div>
    



<script>
	


				
	
function myMap() {
var mapProp= {
	center:new google.maps.LatLng(18.516586, -88.298096),
	zoom:4,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
	
}

	
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3gndIlpbGAQvcj7fydIL83oFwyntJxSw&callback=myMap"></script>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });n
        });
    </script>
	
</body>

</html>
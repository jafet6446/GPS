<!DOCTYPE html>

<html lang="es">
  <head>
    <title>Gps Tracking | Inicio</title>
	  <link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
      <link href="{{ asset('css/Home_Styles.css') }}" rel="stylesheet">
      
      <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
      <script src="https://kit.fontawesome.com/a02c47cedb.js" crossorigin="anonymous"></script>

   
  </head>
    
    <body> 
    <!-- Header -->
        
        <header id="header">
    <!-- Menú de Navegacion -->
        
    <!-- Final del Menú de Navegacion -->
    <!-- Banner -->    
			<nav class="Menu">
            <div class="logo">
                <h1><a href="{{url('home')}}">Gps Tracking System</a></h1>
            </div>
            
            <div class="container_elements">
                <ul class="elements">
					@guest
                    <li><a class="nav-link" href="{{url('/')}}" class="active">Inicio</a></li>
					<li><a class="nav-link" href=" {{ route('tablero.index') }} ">Tablero</a></li>
					<li><a class="nav-link" href="{{ route('lpaquetes.paquetes') }}">Paquetes</a></li>
						
					@if (Route::has('register'))
                    <li><a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a></li>
                    @endif
					@else
					<li><a class="nav-link" href="{{url('home')}}" class="active">Inicio</a></li>
                    <li><a class="nav-link" href=" {{ route('tablero.index') }} ">Tablero</a></li>
					<li><a class="nav-link" href="{{ route('lpaquetes.paquetes') }}">Paquetes</a></li>
					<li><a class="nav-link"href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Cerrar sesión') }}</a></li>
                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                       @endguest
                </ul>
            </div>
            </nav>
		   

        <main class="py-4">
            @yield('content')
        </main>

            <div class="banner">
                <div class="banner_title">
                    <h1> Bienvenido a Gps Tracking System </h1>
                    <hr>
                    <p>La tecnología Gps al alcance de tu mano</p>
                </div>
            </div>
            <div class="Skew_down">
            </div>
    <!-- Final Banner -->        
        </header>
    <!-- Final del Header -->
    
    <!-- Acerca de nosotros -->
        <main>
            <section class="nosotros">
                <div class="info_nosotros">
                    <h1>Acerca de nosotros</h1>
                    <p>La empresa brinda una asistencia tecnológica a todos aquellos que necesiten un servicio de ayuda. El esfuerzo está centrado en brindar respuestas rápidas, precisas y 			eficientes, buscando satisfacer a nuestros clientes con nuestro servicio, asesoramiento, compromiso, solución de posibles robos. 
						Promover el crecimiento sustentable de la organización. Lograr una organización flexible a los cambios que el mercado requiere, anticipar las tendencias para generar propuestas diferenciadoras y variadas.
						Fortalecer en forma constante la inversión en tecnología para el desarrollo de nuestra actividad.
						Generar acciones que estimulen las alianzas con organizaciones que colaboren socialmente.
					</p>
					<br>
					<h3>Facebook: <a href="https://www.facebook.com/GPS-Tracking-System-107306418167590">GPS Tracking System</a></h3>
                </div>
            
            </section>
        
        </main>
        
    <!-- Final Acerca de nosotros -->    
        
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    
    </body>
</html>


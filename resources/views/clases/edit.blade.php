<!DOCTYPE html>

<html>
<head>
	<title>Tipos</title>
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
	

	<link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
</head>
@extends('layouts.app')

@section('content')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auto_estilos.css') }}">
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>Tipos</h3>
			<p>
				Estas en las opciones de administrador, el uso inapropiado de esta sección puede causar errores en el sistema
			</p>
            <form method="get" action="{{ route('clase.index') }}">
              <button class="btn transparent" id="sign-up-btn">Lista de tipos</button> 
		</form>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('clase.update',['id' => $clase->id]) }}">
				@method('PUT')
			  @csrf
				<h2 class="title">Actualizar tipo de vehículo</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Nombre del tipo" name="nombre" required value ="{{$clase->nombre}}">
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
           		    	<select type="text" placeholder="Estado" name="estado" id="estado" class="form-control">
								
								<option value="{{$clase->estado}}">{{$clase->estado}}</option>
								<option value="Activo">Activo</option>
								<option value="Inactivo">Inactivo</option>

			   			</select>
           		    	
            	   </div>
            	</div>
				
            	<input type="submit" class="btn" value="Actualizar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>
	 @endsection
</body>
</html>

<!DOCTYPE html>
<html>
	<head>
		<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Paquetes</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style4.css">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    
	<link href="{{ asset('css/paquetes_estilos.css') }}" rel="stylesheet">
	<link href="{{ asset('css/Home_Styles.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
		
    
</head>
<body>
	 <!-- Menú de Navegacion -->
        
    <!-- Final del Menú de Navegacion -->
    <!-- Banner -->    
			<nav class="Menu">
            <div class="logo">
                <h1><a href="{{url('home')}}">Gps Tracking System</a></h1>
            </div>
            
            <div class="container_elements">
                <ul class="elements">
					@guest
                    <li><a class="nav-link" href="{{url('/')}}" class="active">Inicio</a></li>
					<li><a class="nav-link" href=" {{ route('tablero.index') }} ">Tablero</a></li>
					<li><a class="nav-link" href="{{ route('lpaquetes.paquetes') }}">Paquetes</a></li>
						
					@if (Route::has('register'))
                    <li><a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a></li>
                    @endif
					@else
					<li><a class="nav-link" href="{{url('home')}}" class="active">Inicio</a></li>
                    <li><a class="nav-link" href=" {{ route('tablero.index') }} ">Tablero</a></li>
					<li><a class="nav-link" href="{{ route('lpaquetes.paquetes') }}">Paquetes</a></li>
					<li><a class="nav-link"href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Cerrar sesión') }}</a></li>
                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                       @endguest
                </ul>
            </div>
            </nav>

    
            
          
<div id="tabla-precios">
<h1 align="center">Paquetes</h1>
	
	@foreach($paquetes as $paquete)
    <div class="precio-col" style=" width: 450px; height: 600px">
     <div class="precio-col-header">
     <h3>{{$paquete->precio}}/{{$paquete->duracion}} + $600 x GPS</h3>
     <p>{{$paquete->nombre}}</p>
     </div>
    
    <div class="precio-col-features">
     <p>Vehículos: {{$paquete->vehiculos}}</p>
     <p>{{$paquete->caract_1}}</p>
     <p>{{$paquete->caract_2}}</p>
	 <p>{{$paquete->caract_3}}</p>
	 <p>{{$paquete->caract_4}}</p>
	 <p>{{$paquete->caract_5}}</p>
     </div>
    
    <div class="precio-col-comprar">
     <a href="{{ route('formulario.show', ['id'=>$paquete->id]) }}">Solicitar</a>
     </div>
     </div>
    @endforeach
    

    </div>
</body>
</html>
<!DOCTYPE html>
<title>Pefil de Usuario</title>

<html lang="en">
  <head>
	  
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
    
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    
  </head>
	@extends('layouts.app')

	@section('content')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/perfil_estilos.css') }}">
  <body>
    <header class="head">
        <div class="title">
            <h1 align="center">Configuración de usuario</h1>
        </div>
    </header>

	
      <div class="row">
  
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 toppad" >
   
   
          <div class="panel panel-success"><br>
              <h2 class="panel-title">Actualizar perfil</h2>
			<form method="post" action="{{ route('perfil.store') }}">
			@csrf
            <div class="panel-body">
              <div class="row">
                <div class=" col-md-9 col-lg-9 "> 
                  
					  	<label>Nombre</label>
                        <input type="text" class="form-control " value="{{$usuario->name}}" readonly>
                        <label>Correo electrónico</label>
						<input type="email" class="form-control "  value="{{$usuario->email}}" readonly>
						<label>Contraseña</label>
						<div>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Ingresar nueva contraseña">
						<input type="button" class="form-control input-sm" onclick="mostrarContrasena()" value="Mostrar contraseña" readonly>
						</div>
                        <label>Telefono</label>
                        <input type="text" class="form-control"  value="{{$usuario->phone}}" readonly>
                   
                  
                </div>
              </div>
            </div>
            
            <div class="panel-footer text-center">
                <button type="submit" >Actualizar</button>
            </div>
			</form>
          </div>
        </div>
		
      </div>
    </div>

  </body>
</html>

<script>
  function mostrarContrasena(){
      var tipo = document.getElementById("password");
      if(tipo.type == "password"){
          tipo.type = "text";
      }else{
          tipo.type = "password";
      }
  }
</script>
@endsection

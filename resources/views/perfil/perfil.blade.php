<!DOCTYPE html>
<title>Pefil de Usuario</title>

<html lang="en">
  <head>
	  
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
    
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    
  </head>
	@extends('layouts.app')

	@section('content')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/perfil_estilos.css') }}">
  <body>
    <header class="head">
        <div class="title">
            <h1 align="center">Configuración de usuario</h1>
        </div>
    </header>

	
      <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 toppad" >
   
   
          <div class="panel panel-success"><br>
              <h2 class="panel-title">Perfil</h2>

            <div class="panel-body">
              <div class="row">
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-condensed" >
                        <tbody>
                      <tr>
                        <td>Nombre:</td>
                        <td><input type="text" class="form-control input-sm" name="name" value="{{$usuario->name}}" readonly></td>
                      </tr>
                      <tr>
                        <td>Correo electrónico:</td>
                        <td><input type="email" class="form-control input-sm" name="ocupacionemail" value="{{$usuario->email}}" readonly></td>
                      </tr>
                      <tr>
                        <td>Contraseña:</td>
                        <td><input type="password"  class="form-control input-sm" name="password" value="{{$usuario->password}}" readonly></td>
                      </tr>
					  <tr>
                        <td>Telefono:</td>
                        <td><input type="tel" class="form-control input-sm" required name="phone" value="{{$usuario->phone}}" readonly></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            
            <div class="panel-footer text-center">
                <button type="submit" style="width: 200px" ><a href="{{route('perfil.actualizar')}}">Cambiar contraseña</a> </button>
            </div>
          </div>
        </div>
		</form>
      </div>
    </div>

  </body>
</html>
@endsection

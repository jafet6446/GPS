<!DOCTYPE html>

<html lang="es">
  <head>
	  <title>Solicitudes</title>
<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
	
	  
	  <link href="{{ asset('css/lista.css') }}" rel="stylesheet">
	@extends('layouts.app')

	@section('content')
	<style>
	button{
    width: 100px;
    height: 45px;
    margin: 0 10px;
    background: rgb(0,212,255);
    background: linear-gradient(90deg, rgba(0,212,255,1) 0%, rgba(2,101,255,1) 57%, rgba(0,212,255,1) 100%);
    border-radius: 30px;
    border: 0;
    outline: none;
    color: #fff;
    cursor: pointer;
	
    
		}
	
	</style> 
	  
   
  </head>
  <body style="background: #004e92;
    background: -webkit-linear-gradient(to right, #000428,#004e92);
    background: linear-gradient(to right, #000428, #004e92);">

     
      <section class="Form my-4 mx-5">
                
				  <br>
              <h2 style="color:#FFFFFF"> {{ __('Solicitudes') }}</h2>
			  <br>

				  <br></br>
				  @if($formularios)
				  <table class="table-bordered .table-responsive">
                      <thead>
                        <tr align="center">
						  
                          <th class="text-light">Nombre</th>
                          <th class="text-light">Correo</th>
							<th class="text-light">Telefono</th>
							<th class="text-light">Paquete</th>
						  <th class="text-light">Acciones</th>
							<th class="text-light">Eliminar</th>
                        </tr>
                      </thead>
					  <tbody>
				  	@foreach($formularios as $formulario)
				  			<tr>
                          <td>{{$formulario->nombre}}</td>
                          <td>{{$formulario->correo}}</td>
						  <td>{{$formulario->telefono}}</td>
						  <td>{{$formulario->paquete->nombre}}</td>
							<td >
								<a href="{{route('usuarios.create', ['id' => $formulario->id])}}"><button>Registrar</button></a>
							</td>
						 <td style="text-align: center">
							<form method ="post" action="{{route('formulario.delete', ['id' => $formulario->id]) }}">
								@method('DELETE')
								@csrf
								<button style="background:#7B1414">Eliminar</button>
							</form>
						</td>
						  </tr>
							
				  	@endforeach
						</tbody>
                    </table>
				  @else
				  	
				  		No se encuentran clases registradas
				 	 
				  @endif
				  
       
      </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  @endsection
  </body>
</html>
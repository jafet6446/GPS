<!DOCTYPE html>

<html>
<head>
	<title>Contactanos</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auto_estilos.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>Formulario</h3>
			<p>
				Favor de llenar el formulario con datos personales, nuestro equipo de venta se encargara de verificar su solicitud.
			</p>
            <br>
			<form method="get" action="{{ route('lpaquetes.paquetes') }}">
              <button class="btn transparent" id="sign-up-btn">Paquetes</button> 
			</form>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('formulario.store') }}">
			  @csrf
				<h2 class="title">Formulario</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Nombre completo" name="nombre" required >
           		   </div>
           		</div>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Correo" name="correo" required >
           		   </div>
           		</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero telefonico" name="telefono" required >
           		   </div>
           		</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<select type="text" name="paquete_id" id="paquete_id" class="form-control" required>						
								<option value="{{$paquete->id}}">{{$paquete->nombre}}/{{$paquete->duracion}}</option>

			   			</select>
           		   </div>
           		</div>
				
            	<input type="submit" class="btn" value="Enviar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>

</body>
</html>

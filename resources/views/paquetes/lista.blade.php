<!DOCTYPE html>

<html lang="es"><head>
	  <title>Paquetes</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">

	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
	<link href="{{ asset('css/lista.css') }}" rel="stylesheet">
	  
@extends('layouts.app')

@section('content')
	  
	<style>
	button{
    width: 100px;
    height: 45px;
    margin: 0 10px;
    background: rgb(0,212,255);
    background: linear-gradient(90deg, rgba(0,212,255,1) 0%, rgba(2,101,255,1) 57%, rgba(0,212,255,1) 100%);
    border-radius: 30px;
    border: 0;
    outline: none;
    color: #fff;
    cursor: pointer;
	
    
		}
	
	</style> 
	  
   
  </head>
  <body style="background: #004e92;
    background: -webkit-linear-gradient(to right, #000428,#004e92);
    background: linear-gradient(to right, #000428, #004e92);">

     
      <section class="Form my-4 mx-5">
                
				  <br>
              <h2 style="color:#FFFFFF" > {{ __('Paquetes') }}</h2>
			  <br>
              <a href="{{ route('paquetes.create') }}"><button style="width: 140px; height: 50px; background: linear-gradient(to right, #FF6669, #7B0002); ">Agregar paquete</button></a>
				  <br></br>
				  @if($paquetes)
				  <table class="table-bordered .table-responsive" >
                      <thead>
                        <tr>
						  
                          <th class="text-light">Nombre</th>
						  <th class="text-light">Duración</th>
                          <th class="text-light">Precio</th>
						  <th class="text-light">Numero de vehiculos</th>
						  <th class="text-light">C_1</th>
						  <th class="text-light">C_2</th>
						  <th class="text-light">C_3</th>
						  <th class="text-light">C_4</th>
						  <th class="text-light">C_5</th>
						  <th class="text-light">Estado</th>
						  <th class="text-light">Acciones</th>
                        </tr>
                      </thead>
					  <tbody>
				  	@foreach($paquetes as $paquete)
				  		<tr>
                          
                            <td>{{$paquete->nombre}}</td>
							<td>{{$paquete->duracion}}</td>
							<td>{{$paquete->precio}}</td>
							<td>{{$paquete->vehiculos}}</td>
							<td>{{$paquete->caract_1}}</td>
							<td>{{$paquete->caract_2}}</td>
							<td>{{$paquete->caract_3}}</td>
							<td>{{$paquete->caract_4}}</td>
							<td>{{$paquete->caract_5}}</td>
                            <td>{{$paquete->estado}}</td>
							<td >
								<a href="{{route('paquetes.edit', ['id' => $paquete->id]) }}"><button>Actualizar</button></a>
							</td>
						  </tr>
							
				  	@endforeach
						</tbody>
                    </table>
				  @else
				  	
				  		No se encuentran clases registradas
				 	 
				  @endif
				  
       
      </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  @endsection
  </body>
</html>
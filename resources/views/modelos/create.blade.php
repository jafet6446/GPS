<!DOCTYPE html>
<title>Modelos</title>
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
@extends('layouts.app')

@section('content')
<html>
<head>
	<title>Modelos</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auto_estilos.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>Modelo</h3>
			<p>
				Estas en las opciones de administrador, el uso inapropiado de esta sección puede causar errores en el sistema.
			</p>
            
			<form method="get" action="{{ route('modelo.index') }}">
              <button class="btn transparent" id="sign-up-btn">Lista de modelos</button> 
		</form>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('modelo.store') }}">
			  @csrf
				<h2 class="title">Registra un modelo</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Nombre del modelo" name="nombre" required >
           		   </div>
           		</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<select type="text" name="tipo" id="tipo" class="form-control" required>
							<option value=" ">Seleccionar tipo de vehículo</option>
							 @foreach ($clases as $clase)
								<option value="{{$clase->id}}">{{$clase->nombre}}</option>
							 @endforeach
					
			   			</select>
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
           		    	<select type="text" placeholder="Clase" name="estado" id="estado" class="form-control">
							<option value="Activo">Activo</option>
							<option value="Inactivo">Inactivo</option>
			   			</select>

            	   </div>
            	</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<select type="text" name="marca" id="marca" class="form-control" required>
							<option value=" ">Seleccionar marca</option>
							 @foreach ($marcas as $marca)
								<option value="{{$marca->id}}">{{$marca->nombre}}</option>
							 @endforeach
					
			   			</select>
           		   </div>
           		</div>
				
            	<input type="submit" class="btn" value="Registrar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>
	@endsection
</body>
</html>

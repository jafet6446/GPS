<!DOCTYPE html>
<title>GPS</title>
<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
@extends('layouts.app')

@section('content')
<html>
<head>
	<title>GPS</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auto_estilos.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>GPS</h3>
			<p>
				Estas en las opciones de administrador, el uso inapropiado de esta sección puede causar errores en el sistema.
			</p>
            
			<form method="get" action="{{ route('gps.index') }}">
              <button class="btn transparent" id="sign-up-btn">Lista de GPS</button> 
		</form>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('gps.store') }}">
			  @csrf
				<h2 class="title">Registra un GPS</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero de serie" class="form_content form-control @error('numero_serie') is-invalid @enderror" name="numero_serie" required value="{{ old('numero_serie') }}" >
					   @if($errors->has('numero_serie'))
						<br>
					   <br>
						<p class="help-block">{{ $errors->first('numero_serie') }}</p>
					
						@endif
					   
           		   </div>
           		</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero de telefono" class="form_content form-control @error('numero_telefono') is-invalid @enderror" name="numero_telefono" required value="{{old('numero_telefono') }}" >
					  
                   
					   @if($errors->has('numero_telefono'))
						<br>
					   <br>
						<p class="help-block">{{ $errors->first('numero_telefono') }}</p>
					
						@endif
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
           		    	<select type="text" placeholder="Clase" name="estado" id="estado" class="form-control">
							<option value="Activo">Activo</option>
							<option value="Inactivo">Inactivo</option>
			   			</select>

            	   </div>
            	</div>
				<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
           		    	<select type="text" placeholder="vehiculo_id" name="vehiculo_id" id="vehiculo_id" class="form-control">
							<option value="{{ $vehiculo->id }}">{{$vehiculo->modelo->clase->nombre}}/{{$vehiculo->modelo->marca->nombre}}/{{$vehiculo->modelo->nombre}}/{{$vehiculo->anio}}</option>
							
			   			</select>

            	   </div>
            	</div>
				
            	<input type="submit" class="btn" value="Registrar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>
	@endsection
</body>
</html>

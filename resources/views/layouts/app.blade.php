<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GPS Tracking System') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!--<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">-->

    <!-- Styles -->
    <!--<link href="{{ asset('css/app.') }}" relcss="stylesheet">-->
	
	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
     
      
     <script src="https://kit.fontawesome.com/a02c47cedb.js" crossorigin="anonymous"></script>
	

	<!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style4.css">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="CSS/DashStyles.css"> 

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
	  
	   <link href="{{ asset('css/DashStyles.css') }}" rel="stylesheet">
	
</head>
<body>
    
		
     <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>GPS Tracking System</h3>
                <strong>GTS</strong>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                    <a href="{{url('home')}}">
                        <i class='bx bx-home'></i>
                        Inicio
                    </a>
                </li>
				
				@if(Auth::user()->hasRol('Usuario'))
                <li>
                    <a href="{{ route('perfil.index') }}">
                        <i class='bx bx-user'></i>
                        Perfil
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Usuario'))
                <li>
                    <a href="{{ route('vehiculo.index') }}">
                        <i class='bx bx-car'></i>
                        Mis Vehículos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Usuario'))
                <li>
                    <a href="{{ route('emergencias.index') }}">
                        <i class='bx bx-phone-call bx-tada nav_icon'></i>
                        Emergencia 
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				
				 <li>
                    <a href="{{ route('clase.index') }}">
                        <i class='bx bx-car'></i>
                        Tipos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('marca.index') }}">
                        <i class='bx bx-car'></i>
                        Marcas
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('modelo.index') }}">
                        <i class='bx bx-car'></i>
                        Modelos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('paquetes.index') }}">
                        <i class='bx bx-car'></i>
                        Paquetes
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('notificacion.index') }}">
                        <i class='bx bx-car'></i>
                        Notificaciones
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('emergencias.lista') }}">
                        <i class='bx bx-car'></i>
                        Emergencias
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('usuarios.index') }}">
                        <i class='bx bx-car'></i>
                        Usuarios
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('formulario.index') }}">
                        <i class='bx bx-car'></i>
                        Solicitudes
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('gps.vehiculos') }}">
                        <i class='bx bx-car'></i>
                        Vehículos
                    </a>
                </li>
				@endif
				@if(Auth::user()->hasRol('Administrador'))
				<li>
                    <a href="{{ route('gps.index') }}">
                        <i class='bx bx-car'></i>
                        GPS
                    </a>
                </li>
				@endif
            </ul>
		
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class='bx bx-log-out nav_icon'></i> {{ __('Cerrar sesión') }}</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                       @csrf
                    </form>
					<br>
					<br>
					<br>
					<br>	
					<br>
					<br>
					<br>
					
				@if(Auth::user()->hasRol('Usuario'))
					<br>
					<br>
					<br>
					<br>	
					
				
                   @endif                
        </nav>
					<main class="py-4">
						@yield('content')
					</main>
				
        </div>
		   
	
        
    </div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	  <link href="{{ asset('css/Estilos_Form_prueba2.css') }}" rel="stylesheet">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
    <title>Formulario de Usuario</title>
<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
  </head>
  <body>
    <div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          <form method="POST" action="{{ route('usuarios.store') }}">
			  @csrf
            <h2 class="title">Registrarse</h2>
            <div class="input-field">
            <i class='bx bxs-id-card'></i>
           
			<input id="name" type="text" class="form_content form-control @error('name') is-invalid @enderror" name="name" value="{{ $usuario->nombre }}" placeholder="Ingrese su nombre" required autocomplete="name" autofocus>
				@if($errors->has('name'))
						<br>
						<p class="help-block">{{ $errors->first('name') }}</p>
					
					@endif
          </div>
            <!--<div class="input-field">
              <i class='bx bxs-user'></i>
              <input type="text" placeholder="Nombre de Usuario" />
            </div>-->
            <div class="input-field">
              <i class='bx bx-mail-send'></i>
              <input id="email" type="email" class="form_content2 form-control @error('email') is-invalid @enderror" name="email" placeholder="Ingrese un correo" value="{{ $usuario->correo }}"  autocomplete="email">
						  @if($errors->has('email'))
						<br>
						<p class="help-block">{{ $errors->first('email') }}</p>
					
						@endif
				
            </div>
            <div class="input-field">
              <i class='bx bxs-lock-open-alt'></i>
              <input id="password" type="password" class="form_content2 form-control @error('password') is-invalid @enderror" name="password" placeholder="Ingrese una contraseña"  autocomplete="new-password">
				  @if($errors->has('password'))
						<br>
						<p class="help-block">{{ $errors->first('password') }}</p>
					
					@endif
							
						 
            </div>
            <div class="input-field">
            <i class='bx bxs-lock-alt'></i>
            <input id="password_confirmation" type="password" class="form_content2 form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Ingrese una contraseña"  autocomplete="new-password">
							
						  
          </div>
          <div class="input-field">
            <i class='bx bxs-phone'></i>
            <input id="phone" type="phone" class="form_content2 form-control @error('phone') is-invalid @enderror" name="phone" placeholder="Ingrese un teléfono" value="{{ $usuario->telefono }}"  autocomplete="phone">
			  				@if($errors->has('phone'))
								<br>
								<p class="help-block">{{ $errors->first('phone') }}</p>
					
							@endif
          </div>
			<input type="hidden" name = "paquete_id" value="{{ $usuario->paquete->id }}"  autocomplete="phone">
            <input type="submit" class="btn" value="Registrar"/>
          </form>
        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
            <h2>Advertencía</h2>
            <p>
              Estas en las opciones de administrador, el uso inapropiado de esta sección puede causar errores en el sistema, verifica con el cliente los datos a guardar para prevenir errores en los datos.
            </p>
            <form method="GET" action="{{route('formulario.index')}}"> 
				<button class="btn transparent" id="sign-up-btn">
				  Solicitudes
				</button>
			 </form>
          </div>
          <img src="img/InicioSesion.svg" class="image" alt=""/>
        </div>
      </div>
    </div>

    
  </body>
</html>


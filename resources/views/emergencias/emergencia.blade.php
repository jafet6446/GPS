<!DOCTYPE html>

<html>
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <title>Emergencias</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style4.css">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    
	<link href="{{ asset('css/paquetes_estilos.css') }}" rel="stylesheet">
	<link href="{{ asset('css/Home_Styles.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
		
    
</head>
@extends('layouts.app')

@section('content')
<body>
      

<div id="tabla-precios">
<h1 align="center">Emergencias</h1>
	
    @foreach($emergencias as $emergencia)
     <div style=" width: 450px; height:auto" class="precio-col">
     <div class="precio-col-header">
     <h3>{{$emergencia->nombre}}</h3>
     
     </div>
    
     <div class="precio-col-features">
	 <br>
	 <h1 align="center">{{$emergencia->numero}}</h1>
     <p>{{$emergencia->descripcion}}</p>
     </div>
     

    </div>
	
	@endforeach
	</div>
	@endsection
</body>
</html>
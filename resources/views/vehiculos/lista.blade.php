	<!DOCTYPE html>


<html lang="es"><head>
	  <title>Vehículos</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
	
	   
	  
@extends('layouts.app')

@section('content')
	
<link href="{{ asset('css/lista.css') }}" rel="stylesheet">

	<style>
		#{
			margin: 0;
			padding: 0;
			box-sizing: border-box;
		}
	button{
    width: 100px;
    height: 45px;
    margin: 0 10px;
 
	background: #133b5c;
    border-radius: 30px;
    border: 0;
    outline: none;
    color: #fff;
    cursor: pointer;
	
    
		}
		td{
			background:#DDDDDD;
		}
		
	</style> 
	  
   
  </head>
	
  <body style="background:#EEEEEE">

     
			
				 
	  	
              <h2  align="center"> {{ __('Mis vehículos') }}</h2>
		
			  <br>
              <a href="{{ route('vehiculo.create') }}"><button style="width: auto; height: 50px;background: #12192C;">Agregar vehículo</button></a>
				  <br></br>
				  @if($vehiculos->count())
				  <table class="table-bordered .table-responsive">
                      <thead>	
                        <tr align="center">
						  <th class="text-light">Numero de serie</th>
						  <th class="text-light">Placa</th>
                          <th class="text-light">Marca</th>
                          <th class="text-light">Modelo</th>
						  <th class="text-light">Tipo</th>
						  <th class="text-light">Año</th>
                          <th class="descripcion text-light">Descripción</th>
						  <th class="text-light">GPS</th>
                          <th class="text-light" >Acciones</th>
						  <th class="text-light">Eliminar</th>
                        </tr>
                      </thead>
					  <tbody>
				  	@foreach($vehiculos as $vehiculo)
				  		<tr>
							@if($vehiculo->usuario_id == $usuario->id)
                          <td>{{$vehiculo->numeroSerie}}</td>
						  <td>{{$vehiculo->placa}}</td>
                          <td>{{$vehiculo->marca->nombre}}</td>
                          <td>{{$vehiculo->modelo->nombre}}</td>
						  <td>{{$vehiculo->modelo->clase->nombre}}</td>
                          <td>{{$vehiculo->anio}}</td>
						  <td ><textarea readonly id="description" name="description" rows="5" style="background: #DDDDDD">{{$vehiculo->descripcion}}</textarea></td>
						  <td>@if($vehiculo->gps_id == null)
							  
							  @else
								{{$vehiculo->gps->numero_telefono}}
							  @endif
						  </td>
                          <td style="text-align: center"><div>
							  @if($vehiculo->gps_id == null)
							  
							  @else
							  <a href="{{route('tablero.mapa', ['id' => $vehiculo->gps_id])}}"><button>Localizar</button></a>
							  @endif
							  <a href="{{route('vehiculo.edit', ['id' => $vehiculo->id]) }}"><button>Actualizar</button></a>

							  </td>
							  <td style="text-align: center">
							  <form method ="post" action="{{route('vehiculo.delete', ['id' => $vehiculo->id]) }}">
									@method('DELETE')
									@csrf
									<button style="background:#7B1414">Eliminar</button>
							  </form></div></td>
						  </tr>
							@endif
				  	@endforeach
						</tbody>
                    </table>
				  @else
					<h1>No hay vehículos aún</h1>
				  @endif
       
      </section>
        </div>
    
	  
      

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  @endsection
  </body>
</html>




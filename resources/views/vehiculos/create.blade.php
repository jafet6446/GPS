<!DOCTYPE html>

<html>
<head>

	<title>Vehiculos</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
</head>

@extends('layouts.app')

@section('content')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auto_estilos.css') }}">
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>¿Ya tienes registrado un vehículo?</h3>
			<p>
				Los datos regitrados a continuación son esenciales para poder 
				tener un mejor control del vehiculo que desea gestionar, esto 
				ayuda para saber que vehiculo esta localizando.
			</p>
             <form method="get" action="{{ route('vehiculo.index') }}">
              <button class="btn transparent" id="sign-up-btn">Mis Vehiculos</button> 
		</form>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('vehiculo.store') }}">
			  @csrf
				<h2 class="title">Registra tu vehículo</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero de serie" name="numeroserie" required >
           		   </div>
           		</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Placa" name="placa" required >
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
           		    	
           		    	<select type="text" name="marca" id="marca_id" class="form-control" required>
							<option value=" ">Seleccionar marca</option>
							 @foreach ($marcas as $marca)
								<option value="{{$marca->id}}">{{$marca->nombre}}</option>
							 @endforeach
					
			   			</select>
            	   </div>
            	</div>
				
				
				<div class="input-div pass">
					<div class="i"> 
						<i class='bx bxs-package'></i>
					</div>
					<div class="div">
						 <select type="text" name="modelo" id="modelo_id" class="form-control" required>
							<option value=" ">Seleccionar modelo</option>
							<option value=" ">Debes seleccionar una marca primero</option>
			   			</select>
						 
				 </div>
			  </div>
				
				
			  <div class="input-div pass">
				<div class="i"> 
					<i class='bx bx-calendar'></i>
				</div>
				<div class="div">
					 
					 <input type="text" placeholder="Año" name="anio" required>
			 </div>
		  </div>
	  <div class="input-div pass">
		  <i class='bx bxs-comment-detail'></i> 
		<div class="div">
			<textarea type="text" placeholder="Descripción" id="description" name="descripcion" rows="5"></textarea>
	 </div>
  </div>
				<input type="hidden"  name="usuarioId" required value="{{$usuario -> id}}">
            	<input type="submit" class="btn" value="Registrar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>
	 
</body>
</html>
@endsection

<script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
<script>

    $(document).ready(function() {
        $('#marca_id').change(function () {
            $.get("{{ url('dropdown')}}",
                {option: $(this).val()},
                function (data) {
                    $('#modelo_id').empty();
                    $('#modelo_id').append("option value=''>Seleccione el modelo</option>")
                    $.each(data, function (key, element) {
                        $('#modelo_id').append("<option value='" + key + "'>" + element + "</option>");
                    });
                });
        });

    });

</script>


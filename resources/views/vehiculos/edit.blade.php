<!DOCTYPE html>
<html>
<head>
	<link href="{{ asset('Img/Logo32.png') }}" rel="icon">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
	@extends('layouts.app')

	@section('content')
	<title>Vehiculos</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auto_estilos.css') }}">
	
</head>
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>Advertencía</h3>
			<p>
				Estas apunto de hacer una actualización a tu vehículo ya registrado, verifica que los nuevos datos ingresados sean correctos
			</p>
           <form method="get" action="{{ route('vehiculo.index') }}">
              <button class="btn transparent" id="sign-up-btn">Mis Vehiculos</button> 
		</form>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('vehiculo.update',['id' => $vehiculo->id]) }}">
				@method('PUT')
			  @csrf
				<h2 class="title">Actualiza tu vehículo</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero de serie" name="numeroserie" required value ="{{$vehiculo->numeroSerie}}">
           		   </div>
           		</div>
				<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Placa" name="placa" required value ="{{$vehiculo->placa}}">
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
					   <select type="text" placeholder="Marca" name="marca" id="marca" class="form-control">
							<option value="{{$vehiculo->marca_id}}">{{$vehiculo->marca->nombre}}</option>
								 @foreach ($marcas as $marca)
									<option value="{{$vehiculo->marca_id}}">{{$marca->nombre}}</option>
								 @endforeach
           		       </select>
            	   </div>
            	</div>
				<div class="input-div pass">
					<div class="i"> 
						<i class='bx bxs-package'></i>
					</div>
					<div class="div">
						 <select type="text" name="modelo" id="modelo" class="form-control" required>
							<option value="{{$vehiculo->modelo_id}}">{{$vehiculo->modelo->nombre}}</option>
							 @foreach ($modelos as $modelo)
								<option value="{{$modelo->nombre}}">{{$modelo->nombre}}</option>
							 @endforeach
					
			   			</select>
						 
				 </div>
			  </div>
			  <div class="input-div pass">
				<div class="i"> 
					<i class='bx bx-calendar'></i>
				</div>
				<div class="div">
					 
					 <input type="text" placeholder="Año" name="anio" required value ="{{$vehiculo->anio}}">
			 </div>
		  </div>
		 
			
	  <div class="input-div pass">
		  <i class='bx bxs-comment-detail'></i> 
		<div class="div">
			<textarea type="text" placeholder="Descripción" id="description" name="descripcion">{{$vehiculo->descripcion}}</textarea>
	 </div>
  </div>
				<input type="hidden"  name="usuarioId" required value="{{$usuario -> id}}">
            	<input type="submit" class="btn" value="Actualizar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>
	@endsection
</body>
</html>

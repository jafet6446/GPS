<?php

use Illuminate\Database\Seeder;
use App\Rol;
use App\User;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		$rol = new Rol();
		$rol->nombre = "Administrador";
		$rol->save();
		
		$id = $rol->id;
		
		$rol = new Rol();
		$rol->nombre = "Usuario";
		$rol->save();
		
		$user = new User();
		$user->name = "Jafet Emmanuel Cime Sanchez";
		$user->email = "jafet6446@gmail.com";
		$user->password = bcrypt("jafet123");
		$user->phone = "9832103246";
		$user->rol_id = $id;
		$user->save();
    }
}

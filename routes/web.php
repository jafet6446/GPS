<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Rutas tablero y mapa
Route::get('/tablero', 'TableroController@index')->name('tablero.index');
Route::get('/mapa/{id}', 'TableroController@mapa')->name('tablero.mapa');

//Ruta para el perfil del usuario
Route::get('/perfil', 'PerfilController@index')->name('perfil.index');
Route::get('/perfil/actualizar', 'PerfilController@actualizar')->name('perfil.actualizar');
Route::post('/perfil/actualizar/password', 'PerfilController@store')->name('perfil.store');

//Route::get('/lpaquetes', 'LPaquetesController@index')->name('lpaquetes.index');

//Ruta para el select dinamica
Route::get('dropdown', 'MarcaController@modelos');

//Rutas GPS
Route::get('/Vehiculos/lista', 'GpsController@vehiculos')->name('gps.vehiculos');
Route::get('/GPS/lista', 'GpsController@index')->name('gps.index');
Route::get('/GPS/nuevo{id}', 'GpsController@create')->name('gps.create');
Route::post('/GPS/registrar', 'GpsController@store')->name('gps.store');
Route::get('/GPS/editar/{id}', 'GpsController@edit')->name('gps.edit');
Route::put('/GPS/editar/{id}', 'GpsController@update')->name('gps.update');

//Rutas paquetes
Route::get('/paquetes/lista', 'PaquetesController@index')->name('paquetes.index');
Route::get('/paquetes/nuevo', 'PaquetesController@create')->name('paquetes.create');
Route::post('/paquetes/registrar', 'PaquetesController@store')->name('paquetes.store');
Route::get('/paquetes/editar/{id}', 'PaquetesController@edit')->name('paquetes.edit');
Route::put('/paquetes/editar/{id}', 'PaquetesController@update')->name('paquetes.update');

//Rutas numero de emergencias
Route::get('/emergencias', 'EmergenciasController@index')->name('emergencias.index');
Route::get('/emergencias/lista', 'EmergenciasController@lista')->name('emergencias.lista');
Route::get('/emergencia/nuevo', 'EmergenciasController@create')->name('emergencia.create');
Route::post('/emergencia/registrar', 'EmergenciasController@store')->name('emergencia.store');
Route::get('/emergencia/editar/{id}', 'EmergenciasController@edit')->name('emergencia.edit');
Route::put('/emergencia/editar/{id}', 'EmergenciasController@update')->name('emergencia.update');

//Rutas de usuarios
Route::get('/usuarios', 'UsuariosController@index')->name('usuarios.index');
Route::get('/usuario/nuevo/{id}', 'UsuariosController@create')->name('usuarios.create');
Route::post('/usuario/registrar', 'UsuariosController@store')->name('usuarios.store');

//Rutas formulario
Route::get('/formulario', 'FormularioController@index')->name('formulario.index');
Route::get('/paquetes', 'FormularioController@paquetes')->name('lpaquetes.paquetes');
Route::get('/formulario/nuevo/{id}', 'FormularioController@show')->name('formulario.show');
Route::post('/formulario/registrar', 'FormularioController@store')->name('formulario.store');
Route::delete('/formulario/eliminar/{id}', 'FormularioController@destroy')->name('formulario.delete');

//Rutas vehículos
Route::get('/vehiculo/lista', 'VehiculoController@index')->name('vehiculo.index');
Route::get('/vehiculo/nuevo', 'VehiculoController@create')->name('vehiculo.create');
Route::post('/vehiculo/registrar', 'VehiculoController@store')->name('vehiculo.store');
Route::get('/vehiculo/editar/{id}', 'VehiculoController@edit')->name('vehiculo.edit');
Route::put('/vehiculo/editar/{id}', 'VehiculoController@update')->name('vehiculo.update');
Route::delete('/vehiculo/eliminar/{id}', 'VehiculoController@destroy')->name('vehiculo.delete');

//Rutas clases
Route::get('/clase/lista', 'ClaseController@index')->name('clase.index');
Route::get('/clase/nuevo', 'ClaseController@create')->name('clase.create');
Route::post('/clase/registrar', 'ClaseController@store')->name('clase.store');
Route::get('/clase/editar/{id}', 'ClaseController@edit')->name('clase.edit');
Route::put('/clase/editar/{id}', 'ClaseController@update')->name('clase.update');

//Ruta marcas
Route::get('/marca/lista', 'MarcaController@index')->name('marca.index');
Route::get('/marca/nuevo', 'MarcaController@create')->name('marca.create');
Route::post('/marca/registrar', 'MarcaController@store')->name('marca.store');
Route::get('/marca/editar/{id}', 'MarcaController@edit')->name('marca.edit');
Route::put('/marca/editar/{id}', 'MarcaController@update')->name('marca.update');

//Rutas modelo
Route::get('/modelo/lista', 'ModeloController@index')->name('modelo.index');
Route::get('/modelo/nuevo', 'ModeloController@create')->name('modelo.create');
Route::post('/modelo/registrar', 'ModeloController@store')->name('modelo.store');
Route::get('/modelo/editar/{id}', 'ModeloController@edit')->name('modelo.edit');
Route::put('/modelo/editar/{id}', 'ModeloController@update')->name('modelo.update');

//Ruta notificaciones
Route::get('/notificacion/lista', 'NotificacionesController@index')->name('notificacion.index');
Route::get('/notificacion/nuevo', 'NotificacionesController@create')->name('notificacion.create');
Route::post('/notificacion/registrar', 'NotificacionesController@store')->name('notificacion.store');
Route::get('/notificacion/editar/{id}', 'NotificacionesController@edit')->name('notificacion.edit');
Route::put('/notificacion/editar/{id}', 'NotificacionesController@update')->name('notificacion.update');

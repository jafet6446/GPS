<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::apiResources([
    'usuarios' => 'API\UsuarioController'
]);

Route::apiResources([
    'notificaciones' => 'API\NotificacionController'
]);

Route::apiResources([
    'vehiculos' => 'API\VehiculoController'
]);

Route::apiResources([
    'gps' => 'API\GpsController'
]);
Route::apiResources([
    'sensados' => 'API\SensadoController'
]);
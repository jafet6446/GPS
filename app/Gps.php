<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Gps extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'gps';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'numero_serie','numero_telefono', 'estado', 'vehiculo_id'
    ];
	
	
	public function vehiculo()
   {
    return $this->belongsTo(Vehiculo::class);
   }
}
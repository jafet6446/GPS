<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Paquetes extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'paquetes';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'duracion', 'precio', 'vehiculos', 'caract_1', 'caract_2', 'caract_3', 'caract_4','caract_5', 'estado'
    ];
	
	public function formularios()
  {
    return $this->hasMany(Formulario::class);
  }
}

<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Notificacion extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'notificacion';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'prioridad','estado'
    ];
}
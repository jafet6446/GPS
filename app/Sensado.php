<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Sensado extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'sensado';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'latitud', 'longitud', 'gps_id', 'notificacion_id'
    ];
	public function gps()
  {
    return $this->belongsTo(Gps::class);
  }
	public function notificacion()
  {
    return $this->belongsTo(Notificacion::class);
  }
}

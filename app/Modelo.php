<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Modelo extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'modelo';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'estado', 'clase_id', 'marca_id'
    ];
	
	public function vehiculos()
  {
    return $this->hasMany(Vehiculo::class);
  }
	public function marca()
   {
    return $this->belongsTo(Marca::class);
   }
	public function clase()
   {
    return $this->belongsTo(Clase::class);
   }
}

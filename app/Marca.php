<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Marca extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'marca';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'estado'
    ];
	
	public function vehiculos()
  {
    return $this->hasMany(Vehiculo::class);
  }
	public function modelos()
  {
    return $this->hasMany(Modelo::class);
  }
}

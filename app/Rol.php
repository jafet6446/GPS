<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Rol extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'rol';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre'
    ];
}
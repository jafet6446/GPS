<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Vehiculo extends Mongo
{
	protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'vehiculos';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'numeroSerie', 'placa','marca_id', 'modelo_id', 'anio', 'descripcion', 'fecha_modificacion', 'fecha_registro', 'usuario_id', 'gps_id'
    ];
	
	public function usuario()
   {
    return $this->belongsTo(User::class);
   }
	public function clase()
   {
    return $this->belongsTo(Clase::class);
   }
	public function marca()
   {
    return $this->belongsTo(Marca::class);
   }
	public function modelo()
   {
    return $this->belongsTo(Modelo::class);
   }
	public function gps()
   {
    return $this->belongsTo(Gps::class);
   }
}

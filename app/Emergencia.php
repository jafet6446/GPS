<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Emergencia extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'emergencia';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'numero', 'descripcion', 'estado'
    ];
}
<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Clase extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'clase';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'estado'
    ];
	
	public function vehiculos()
  {
    return $this->hasMany(Vehiculo::class);
  }
}

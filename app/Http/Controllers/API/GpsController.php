<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sensado;
class GpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sensado = new Sensado;
		$sensado->latitud = $request->latitud;
		$sensado->longitud = $request->longitud;
		$sensado->gps_id = $request->gps_id;
		$sensado->notificacion_id = $request->notificacion_id;
		$sensado->save();
    }
}

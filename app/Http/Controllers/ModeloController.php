<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modelo;
use App\Marca;
use App\Clase;
class ModeloController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$modelos = Modelo::all();
		$marcas = Marca::all();
        return view('modelos.lista')->with(compact('modelos'))->with(compact('marcas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$modelos= Modelo::all();
		$marcas = Marca::all();
		$clases = Clase::all();
        return view('modelos.create')->with(compact('modelos'))->with(compact('marcas'))->with(compact('clases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modelo = new Modelo;
		$modelo->nombre = $request->nombre;
		$modelo->clase_id = $request->tipo;
		$modelo->estado = $request->estado;
		$modelo->marca_id = $request->marca;
		$modelo->save();
		$modelo = Modelo::all();
        return redirect()->route('modelo.index');
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$clases = Clase::all();
        $modelo = Modelo::find($id);
		$marcas = Marca::all();
		return view('modelos.edit')->with(compact('modelo'))->with(compact('marcas'))->with(compact('clases'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelo = Modelo::find($id);
		$modelo->nombre = $request->nombre;
		$modelo->clase_id = $request->tipo;
		$modelo->estado = $request->estado;
		$modelo->marca_id = $request->marca;
		$modelo->save();
		return redirect()->route('modelo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Modelo::find($id)->delete();
		return redirect()->route('modelo.index');
    }//
}

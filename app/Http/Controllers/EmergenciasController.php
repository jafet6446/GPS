<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emergencia;
class EmergenciasController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
    {
		$emergencias = Emergencia::All();
        return view('emergencias.emergencia')->with(compact('emergencias'));
    }
	
	public function lista()
    {
		$emergencias = Emergencia::All();
        return view('emergencias.lista')->with(compact('emergencias'));
    }
	
	 public function create()
    {
		
        return view('emergencias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emergencia = new Emergencia;
		$emergencia->nombre = $request->nombre;
		$emergencia->numero = $request->numero;
		$emergencia->descripcion = $request->descripcion;
		$emergencia->estado = $request->estado;
		$emergencia->save();
        return redirect()->route('emergencias.lista');
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
        $emergencia = Emergencia::find($id);
		return view('emergencias.edit')->with(compact('emergencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emergencia = Emergencia::find($id);
		$emergencia->nombre = $request->nombre;
		$emergencia->numero = $request->numero;
		$emergencia->descripcion = $request->descripcion;
		$emergencia->estado = $request->estado;
		$emergencia->save();
        return redirect()->route('emergencias.lista');
    }
}

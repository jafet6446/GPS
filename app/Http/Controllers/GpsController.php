<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Gps;
use App\Vehiculo;
class GpsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$gpss = Gps::all();
        return view('gps.lista')->with(compact('gpss'));
    }
	
	public function vehiculos()
    {
		
		$vehiculos = Vehiculo::All();
        return view('gps.vehiculos')->with(compact('vehiculos'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
		$vehiculo= Vehiculo::find($id);
        return view('gps.create')->with(compact('vehiculo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$request->validate([
			'numero_serie' => 'required|unique:gps',
			'numero_telefono' => 'required|unique:gps',
			'estado' => 'required',
			'vehiculo_id' => 'required',
						  	]);
		
        $gps = new Gps;
		$gps->numero_serie = $request->numero_serie;
		$gps->numero_telefono = $request->numero_telefono;
		$gps->estado = $request->estado;
		$gps->vehiculo_id = $request->vehiculo_id;
		$gps->save();
		$id_gps = $gps->id; 
		
		
		$vehiculo = Vehiculo::find($request->vehiculo_id);
		$vehiculo->gps_id = $id_gps;
		$vehiculo->save();
        return redirect()->route('gps.index');
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
        $gps = Gps::find($id);
		return view('gps.edit')->with(compact('gps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
		
        $gps = Gps::find($id);
		$gps->numero_serie = $request->numero_serie;
		$gps->numero_telefono = $request->numero_telefono;
		$gps->estado = $request->estado;
		$gps->vehiculo_id = $request->vehiculo_id;
		$gps->save();
		return redirect()->route('gps.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Marca::find($id)->delete();
		return redirect()->route('marca.index');
    }//
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehiculo;
use App\Rol;
use App\Gps;
use App\Sensado;
use Carbon\Carbon; 
class TableroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
    {
		$gpss = Gps::all();
		$sensados = Sensado::all();
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return view('mapa.mapa')->with(compact('vehiculos'))->with(compact('usuario'))->with(compact('sensados'))->with(compact('gpss'));
    }
	public function mapa($id)
    {
		
		$gps = Gps::find($id);
		$sensados1 = Sensado::all();
		$sensados = Sensado::orderBy('created_at', 'desc')->where('created_at', '>=', Carbon::today()->startOfMonth())->where('created_at', '<=', Carbon::today()->endOfMonth())->paginate(7);
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return view('tablero.Dash')->with(compact('vehiculos'))->with(compact('usuario'))->with(compact('gps'))->with(compact('sensados'))->with(compact('sensados1'));
    }
}

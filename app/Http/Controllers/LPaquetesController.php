<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paquetes;

class LPaquetesController extends Controller
{
     public function index()
    {
		 $paquetes = Paquetes::All();
        return view('lpaquetes.paquetes')->with(compact('paquetes'));
    }
}

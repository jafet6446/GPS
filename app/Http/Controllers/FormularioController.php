<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paquetes;
use App\Formulario;
class FormularioController extends Controller
{

	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$formularios = Formulario::All();
        return view('formularios.lista')->with(compact('formularios'));
    }
	
	 public function paquetes()
    {
		 $paquetes = Paquetes::All();
        return view('lpaquetes.paquetes')->with(compact('paquetes'));
    }
	
	/*public function create()
    {
		$paquetes = Paquetes::All();
        return view('formularios.create')->with(compact('paquetes'));
    }*/
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paquete = Paquetes::find($id);

		 return view('formularios.show')->with(compact('paquete')); 
    }
	
	public function store(Request $request)
    {
		
        $formulario = new Formulario;
		$formulario->nombre = $request->nombre;
		$formulario->correo = $request->correo;
		$formulario->telefono = $request->telefono;
		$formulario->paquete_id = $request->paquete_id;
		$formulario->save();
		
        return redirect()->route('lpaquetes.paquetes');
		
    }
	public function destroy($id)
    {
        Formulario::find($id)->delete();
		return redirect()->route('formulario.index');
    }
}

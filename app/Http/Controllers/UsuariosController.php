<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Rol;
use App\Formulario;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class UsuariosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$usuarios= User::all();
        return view('usuarios.lista')->with(compact('usuarios'));
    }
	
	public function create($id)
    {
		$usuario = Formulario::find($id);
        return view('usuarios.create')->with(compact('usuario'));
    }

    
	
	public function store(Request $request)
    {
		$request->validate([
			'name' => 'required',
			'email' => 'required|string|email|unique:usuarios',
			'password' => 'required|alpha_num|min:8',
			'phone' => 'required|unique:usuarios',
						  	]);
			
        $usuario = new User;
		$rol = Rol::where('nombre', 'Usuario')->first();
		$id = $rol->id;
		$usuario->name = $request->name;
		$usuario->email = $request->email;
		$usuario->password = bcrypt($request->password);
		$usuario->phone = $request->phone;
		$usuario->rol_id = $id;
		$usuario->paquete_id = $request->paquete_id;
		$usuario->save();
		$usuario= Auth::user();
        return redirect()->route('usuarios.index');
		
		
    }
	
	public function edit($id)
    {
		$usuario= Auth::user();
		return view('usuarios.edit')->with(compact('usuario'));
    }
}

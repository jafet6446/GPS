<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehiculo;
class PerfilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
    {
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return view('perfil.perfil')->with(compact('usuario'));
    }
	
	public function actualizar()
    {
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return view('perfil.actualizar')->with(compact('usuario'));
    }
	  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
    {
		$usuario = Auth::user();
		$usuario->password = bcrypt($request->password);
		$usuario->save();
        return view('perfil.perfil')->with(compact('usuario'));
    }
}

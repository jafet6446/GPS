<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehiculo;
use App\Clase;
use App\Marca;
use App\Modelo;
class VehiculoController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
		$clases = Clase::all();
		$marcas = Marca::all();
		$modelos = Modelo::all();
        return view('vehiculos.lista')->with(compact('vehiculos'))->with(compact('usuario'))->with(compact('clases'))->with(compact('marcas'))->with(compact('modelos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$usuario= Auth::user();
		$clases = Clase::all();
		$marcas = Marca::all();
		$modelos = Modelo::all();
        return view('vehiculos.create')->with(compact('usuario'))->with(compact('clases'))->with(compact('marcas'))->with(compact('modelos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehiculo = new Vehiculo;
		$vehiculo->numeroSerie = $request->numeroserie;
		$vehiculo->placa = $request->placa;
		$vehiculo->marca_id = $request->marca;
		$vehiculo->modelo_id = $request->modelo;
		$vehiculo->anio = $request->anio;
		$vehiculo->descripcion = $request->descripcion;
		$vehiculo->usuario_id = $request->usuarioId;
		$vehiculo->gps_id = null;
		$vehiculo->save();
		
			
        return redirect()->route('vehiculo.index');
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$usuario= Auth::user();
        $vehiculo = Vehiculo::find($id);
		$clases = Clase::all();
		$marcas = Marca::all();
		$modelos = Modelo::all();
		return view('vehiculos.edit')->with(compact('vehiculo'))->with(compact('usuario'))->with(compact('clases'))->with(compact('marcas'))->with(compact('modelos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehiculo = Vehiculo::find($id);
		$vehiculo->numeroSerie = $request->numeroserie;
		$vehiculo->placa = $request->placa;
		$vehiculo->marca_id = $request->marca;
		$vehiculo->modelo_id = $request->modelo;
		$vehiculo->anio = $request->anio;
		$vehiculo->descripcion = $request->descripcion;
		$vehiculo->usuario_id = $request->usuarioId;
		$vehiculo->save();
		return redirect()->route('vehiculo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vehiculo::find($id)->delete();
		return redirect()->route('vehiculo.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notificacion;
class NotificacionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$notificaciones = Notificacion::all();
        return view('notificaciones.lista')->with(compact('notificaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$notificaciones = Notificacion::all();
        return view('notificaciones.create')->with(compact('notificaciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notificacion = new Notificacion;
		$notificacion->nombre = $request->nombre;
		$notificacion->prioridad = $request->prioridad;
		$notificacion->estado = $request->estado;
		$notificacion->save();
        return redirect()->route('notificacion.index');
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
        $notificacion = Notificacion::find($id);
		return view('notificaciones.edit')->with(compact('notificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notificacion = Notificacion::find($id);
		$notificacion->nombre = $request->nombre;
		$notificacion->prioridad = $request->prioridad;
		$notificacion->estado = $request->estado;
		$notificacion->save();
		return redirect()->route('notificacion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Notificacion::find($id)->delete();
		return redirect()->route('notificacion.index');
    }//
}

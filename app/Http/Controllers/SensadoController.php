<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SensadoController extends Controller
{
    public function store(Request $request)
    {
        $sensado = new Sensado;
		$sensado->latitud = $request->latitud;
		$sensado->longitud = $request->longitud;
		$sensado->gps_id = $request->gps_id;
		$sensado->fecha = $request->fecha;
		$sensado->save();
    }
}

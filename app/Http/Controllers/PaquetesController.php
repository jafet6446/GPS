<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Paquetes;

class PaquetesController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
   public function index()
    {
	   $paquetes = Paquetes::All();
        return view('paquetes.lista')->with(compact('paquetes'));
    }
	
	public function create()
    {
		$paquetes = Paquetes::All();
        return view('paquetes.create')->with(compact('paquetes'));
    }
	
	public function store(Request $request)
    {
        $paquete = new Paquetes;
		$paquete->nombre = $request->nombre;
		$paquete->duracion = $request->duracion;
		$paquete->precio = $request->precio;
		$paquete->vehiculos = $request->vehiculos;
		$paquete->caract_1 = $request->caract_1;
		$paquete->caract_2 = $request->caract_2;
		$paquete->caract_3 = $request->caract_3;
		$paquete->caract_4 = $request->caract_4;
		$paquete->caract_5 = $request->caract_5;
		$paquete->estado = $request->estado;
		$paquete->save();
		
        return redirect()->route('paquetes.index');
		
		
    }
	 public function edit($id)
    {
		
        $paquete = Paquetes::find($id);
		return view('paquetes.edit')->with(compact('paquete'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paquete = Paquetes::find($id);
		$paquete->nombre = $request->nombre;
		$paquete->duracion = $request->duracion;
		$paquete->precio = $request->precio;
		$paquete->vehiculos = $request->vehiculos;
		$paquete->caract_1 = $request->caract_1;
		$paquete->caract_2 = $request->caract_2;
		$paquete->caract_3 = $request->caract_3;
		$paquete->caract_4 = $request->caract_4;
		$paquete->caract_5 = $request->caract_5;
		$paquete->estado = $request->estado;
		$paquete->save();
		return redirect()->route('paquetes.index');
    }
}

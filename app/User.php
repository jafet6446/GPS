<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'usuarios';
	
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'rol_id', 'paquete_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	public function vehiculos()
  {
    return $this->hasMany(Vehiculo::class, 'usuario_id');
  }
	public function rol()
   {
    return $this->belongsTo(Rol::class);
   }
	public function paquete()
   {
    return $this->belongsTo(Paquetes::class);
   }
	
	public function hasRol($rol){
		if($this->rol()->where('nombre', $rol)->first()){
			return true;
		}
		return false;
	}
}

<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Mongo;

class Formulario extends Mongo
{
    protected $connection = 'mongodb';
	
	protected $primarykey = '_id';
	
	protected $collection = 'formulario';
	
    //protected $table='vehiculos';
	
	protected $fillable = [
        'nombre', 'correo', 'telefono', 'paquete_id'
    ];
	public function paquete()
   {
    return $this->belongsTo(Paquetes::class);
   }
}